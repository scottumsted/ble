__author__ = 'scottumsted'
from bledb import dbpool

class BleDbHelper():


    def delete_activities(self):
        sql = "delete from activity "
        result = -1
        try:
            con, cur = dbpool.get_connection()
            cur.execute(sql)
            cur.fetchall()
            result = cur.rowcount
        except Exception, e:
            result = -1
        finally:
            con.commit()
            dbpool.put_connection(con)
        return result

    def get_activity(self):
        sql = "select activity_id, server, server_activity_id, activity, client, payload, receipt_date "
        sql += "from activity order by receipt_date desc "
        sql += "limit 10 "
        rows = None
        try:
            con, cur = dbpool.get_connection()
            cur.execute(sql)
            rows = cur.fetchall()
        except Exception, e:
            rows = None
        finally:
            con.commit()
            dbpool.put_connection(con)
        return rows

    def add_activity(self, activity):
        next_sql = "select nextval('activity_id_seq') aid "
        sql = "insert into activity "
        sql += "(activity_id, server, server_activity_id, activity, client, payload, receipt_date) "
        sql += "values "
        sql += "( %(activity_id)s, %(server)s, %(server_activity_id)s, %(activity)s, %(client)s, "
        sql += "%(payload)s, now()) "
        aid = -1
        try:
            con, cur = dbpool.get_connection()
            cur.execute(next_sql)
            rows = cur.fetchall()
            for row in rows:
                aid = row['aid']
                activity.update({'activity_id': aid})
                cur.execute(sql, activity)
                if cur.rowcount < 1:
                    aid = -1
                break
        except Exception, e:
            aid = -1
        finally:
            con.commit()
            dbpool.put_connection(con)
        return aid