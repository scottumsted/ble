
drop table activity;
create table activity
(
activity_id int,
server text,
server_activity_id int,
activity text,
client text,
payload text,
receipt_date timestamp
);

drop index activity_id_idx;
create index activity_id_idx on activity(id);

drop index activity_created_idx;
create index activity_created_idx on activity(receipt_date);

drop sequence activity_id_seq;
create sequence activity_id_seq;
