__author__ = 'scottumsted'
from flask import Flask
from bledb import BleDbHelper

ble_db_helper = BleDbHelper.BleDbHelper()
app = Flask(__name__)
app.config.from_envvar('BLE_SETTINGS')
# set custom app objects here, then import handlers
import ble.views