__author__ = 'scottumsted'
import datetime
from flask.globals import request
from flask import jsonify, json
from flask.templating import render_template
from ble import app, ble_db_helper

def _paddedJsonify(callback, *args, **kwargs):
    content = str(callback) + '(' + json.dumps(dict(*args, **kwargs)) + ')'
    return app.response_class(content, mimetype='application/json')

def get_date_time_string(date_time=None):
    date_format = '%Y%m%d%H%M%S'
    if date_time is None or type(date_time).__name__ != 'datetime':
        date_time = datetime.datetime.now()
    date_time_string = date_time.strftime(date_format)
    return date_time_string

@app.route('/')
def landing():
    return render_template('landing.html')

@app.route('/activity.json', methods=['GET'])
def get_activity():
    result = {'successful': True, 'data': []}
    callback = request.args.get('callback', None)
    try:
        activities = ble_db_helper.get_activity()
        if activities is not None:
            for activity in activities:
                result['data'].append(
                    {
                        'activity_id': activity['activity_id'],
                        'server': activity['server'],
                        'server_activity_id': activity['server_activity_id'],
                        'activity': activity['activity'],
                        'client': activity['client'],
                        'payload': activity['payload'],
                        'receipt_date': get_date_time_string(activity['receipt_date'])
                    }
                )
        if callback is None:
            result = jsonify(result)
        else:
            result = _paddedJsonify(callback, result)
    except Exception, e:
        result['successful'] = False
        result['data'] = 'Failed to locate activity'
    return result

@app.route('/activity.json', methods=['POST'])
def add_activity():
    result = {'successful': True, 'data': []}
    if request.json is not None:
        activity_id = ble_db_helper.add_activity(request.json)
        if activity_id > 0:
            result['successful'] = True
            result['data'] = activity_id
        else:
            result['successful'] = False
            result['data'] = 'Unable to save activity'
    else:
        result['successful'] = False
        result['data'] = 'Request not json'
    callback = request.args.get('callback', None)
    if callback is None:
        result = jsonify(result)
    else:
        result = _paddedJsonify(callback, result)
    return result

@app.route('/delete', methods=['GET'])
def delete_activities():
    result = {'successful': True, 'data': []}
    callback = request.args.get('callback', None)
    try:
        rowcount = ble_db_helper.delete_activities()
        result['data'] = rowcount;
        if callback is None:
            result = jsonify(result)
        else:
            result = _paddedJsonify(callback, result)
    except Exception, e:
        result['successful'] = False
        result['data'] = 'Failed to locate activity'
    return result
